CPU_COUNT ?= 8

setup:
	@ ansible-galaxy install --roles-path ./roles --force --force-with-deps --role-file requirements.yml --verbose

lint:
	@ yamllint --version && yamllint .
	@ ansible-lint --version && ansible-lint site.yml
	@ ansible-playbook site.yml -i inventory/staging --syntax-check

docs-lint:
	@ markdownlint --config .markdownlint.json '*.md'

docker: export BUILD_IMAGE ?= "gitlab-org/ci-cd/shared-runners/images/macstadium/orka/ci"
docker:
	@ docker build -t $(BUILD_IMAGE) -f ./dockerfiles/ci/Dockerfile ./dockerfiles/ci

_orka-check_args:
	@if [ -z "$(ORKA_API_URL)" ]; then echo "Please define ORKA_API_URL"; exit 1; fi
	@if [ -z "$(ORKA_VM_NAME)" ]; then echo "Please define ORKA_VM_NAME"; exit 1; fi

orka-check-vm: _orka-check_args
	@ if [ $(shell ./inventory/orka.py --host "${ORKA_VM_NAME}" | jq length) -ne 1 ]; then echo "'${ORKA_VM_NAME}' VM not found!"; exit 1; fi

orka-create-vm: _orka-check_args
	@ echo "Creating '$(ORKA_VM_NAME)' VM from $(BASE_IMAGE_NAME)..." >/dev/stderr
	./scripts/ci/orka POST resources/vm/create orka_vm_name="$(ORKA_VM_NAME)" \
		orka_base_image="$(BASE_IMAGE_NAME)" orka_image="$(ORKA_VM_NAME)" \
		orka_cpu_core:=$(CPU_COUNT) vcpu_count:=$(CPU_COUNT) >/dev/stderr && \
	./scripts/ci/orka POST resources/vm/deploy orka_vm_name="$(ORKA_VM_NAME)" vnc_console:=true | jq -r .vm_id

orka-provision-gitlab-vm: orka-check-vm
	@ # Provision GitLab image VM with Ansible script
	@ echo "Provisioning '$(ORKA_VM_NAME)' ($(VM_ID)) with base configuration..."
	ansible-playbook base.yml -v -i inventory/orka.py -l "$(ORKA_VM_NAME)" \
		-e ansible_user="$(ANSIBLE_USER)" -e ansible_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e ansible_become_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e ansible_user_updated_password="$(ANSIBLE_USER_UPDATED_PASSWORD)"

orka-provision-toolchain-vm: orka-check-vm
	@ # Provision toolchain VM with Ansible script
	@ echo "Provisioning '$(ORKA_VM_NAME)' ($(VM_ID)) with toolchain..."
	ansible-playbook toolchain.yml -v -i inventory/orka.py -l "$(ORKA_VM_NAME)" \
		-e ansible_user="$(ANSIBLE_USER)" -e ansible_password="$(ANSIBLE_USER_UPDATED_PASSWORD)" \
		-e ansible_become_password="$(ANSIBLE_USER_UPDATED_PASSWORD)"

orka-provision-runner-vm: _orka-check_args
	@ # Provision Runner VM with Ansible script
	@ echo "Provisioning '$(ORKA_VM_NAME)' ($(VM_ID)) with Runner..."
	ansible-playbook runner.yml -v -i inventory/orka.py -l "$(ORKA_VM_NAME)" \
		-e ansible_user="$(ANSIBLE_USER)" -e ansible_password="$(ANSIBLE_USER_UPDATED_PASSWORD)" \
		-e ansible_become_password="$(ANSIBLE_USER_UPDATED_PASSWORD)" \
		-e xcode_version="$(XCODE_VERSION)"

orka-save-vm-image: orka-check-vm
	@ # Save VM state as new image
	@ ./scripts/ci/orka POST resources/image/delete image="$(OUTPUT_IMAGE_NAME)" && \
		echo "Deleted pre-existing image, proceeding to save new image" || \
		echo "Pre-existing image not found, proceeding to save new image"
	@ echo "Rebooting machine to commit changes before saving image"
	ansible "$(ORKA_VM_NAME)" -i inventory/orka.py --user "$(ANSIBLE_USER)" -e ansible_become_password="$(ANSIBLE_USER_UPDATED_PASSWORD)" \
		-e ansible_password="$(ANSIBLE_USER_UPDATED_PASSWORD)" --become --module-name reboot --args "reboot_timeout=180"
	@ echo "Saving image from '$(ORKA_VM_NAME)' ($(VM_ID)) VM to $(OUTPUT_IMAGE_NAME) image..."
	TIMEOUT=1800 ./scripts/ci/orka POST resources/image/save orka_vm_name="$(VM_ID)" new_name="$(OUTPUT_IMAGE_NAME)"

orka-purge-vm: _orka-check_args
	./scripts/ci/orka DELETE resources/vm/purge orka_vm_name="$(ORKA_VM_NAME)"

ci-generate-xcode-pipeline:
	./scripts/ci/generate_xcode_pipeline
