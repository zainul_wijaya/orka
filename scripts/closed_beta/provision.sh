#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

echo "Logging in to 1password"
eval "$(op signin gitlab.1password.com)"

read -rp "Customer Name (lowercase chars for email @closedbeta.com): " CUSTOMER_NAME
read -rp "Issue Link: " ISSUE_URL
read -rp "Xcode Version (eg: 12.2): " XCODE_VERSION
read -rp "Customer runner registration token: " GITLAB_REGISTRATION_TOKEN

echo ""
orka image list | grep "runner-$XCODE_VERSION-202"
echo ""

echo "The latest base image to use should be shown above. If you want to tag a new version from master, run:"
echo ""
cat << EOF
  ORKA_API_URL="\$(jq -r '."api-url"' < ~/.config/configstore/orka-cli.json)" \\
  ORKA_API_TOKEN="\$(jq -r ".token" < ~/.config/configstore/orka-cli.json)" \\
  TIMEOUT=1800 ./scripts/ci/orka POST resources/image/copy \\
    image="runner-$XCODE_VERSION-10.15-beta.img" \\
    new_name="runner-$XCODE_VERSION-\$(date -u '+%Y%m%d').img"
EOF
echo ""

read -rp "Base Image: " BASE_IMAGE_NAME

ORKA_USER="$CUSTOMER_NAME@closedbeta.com"

echo "Creating 1password entry"
op create item login --title "Orka closed beta user - $CUSTOMER_NAME" --url "$ISSUE_URL" --vault Verify --account gitlab --generate-password "username=$ORKA_USER"
ORKA_PASSWORD=$(op get item --vault Verify --account gitlab --fields password "Orka closed beta user - $CUSTOMER_NAME")

echo "Creating orka user"
LICENSE_KEY=$(op get item --vault Verify --account gitlab --fields password "Orka License")
orka user create -y -e "$ORKA_USER" --password "$ORKA_PASSWORD" -l "$LICENSE_KEY"
orka user group --email "$ORKA_USER" --group closedbeta -y

echo "Creating the vm"
docker build -t macos-runner-image-builder -f ./dockerfiles/ci/Dockerfile ./dockerfiles/ci
docker run --rm -it \
  -e ANSIBLE_HOST_KEY_CHECKING=False \
  -e ANSIBLE_USER_UPDATED_PASSWORD="$(op get item --vault Verify --account gitlab --fields password "orka base VM")" \
  -e BASE_IMAGE_NAME="$BASE_IMAGE_NAME" \
  -e CUSTOMER_NAME="$CUSTOMER_NAME" \
  -e GITLAB_REGISTRATION_TOKEN="$GITLAB_REGISTRATION_TOKEN" \
  -e ORKA_API_URL="$(op get item --vault Verify --account gitlab --fields "API URL" "Orka VPN")" \
  -e ORKA_LICENSE_KEY="$LICENSE_KEY" \
  -e ORKA_NO_VPN="true" \
  -e ORKA_PASSWORD="$ORKA_PASSWORD" \
  -e ORKA_USER="$ORKA_USER" \
  -e XCODE_VERSION="$XCODE_VERSION" \
  -v "$(pwd)":/build \
  -w /build \
  macos-runner-image-builder \
  scripts/closed_beta/create_env

echo "Reply to the user in $ISSUE_URL with the following:"
echo "======"
RUNNER_VERSION=$(grep gitlab_runner_version: group_vars/all/gitlab_runner.yml | cut -d " " -f2)
cat << EOF
Hi USER

Your beta MacOS runner is online now! You should be able to execute builds on
macOS, with Xcode $XCODE_VERSION and Runner $RUNNER_VERSION (runner tags: \`macos,macos-10.15,shell,shared-macos,xcode-$XCODE_VERSION\`).

Please remember to reset your runner registration token for security reasons.

Let us know if you have any questions! I'll leave this issue open for future discussions.

/unassign ME
EOF
echo "====="
