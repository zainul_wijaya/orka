#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

echo "Logging in to 1password"
eval "$(op signin gitlab.1password.com)"

ORKA_VPN_USER="$(op get item --vault Verify --account gitlab --fields username "Orka VPN")"
ORKA_VPN_PASS="$(op get item --vault Verify --account gitlab --fields password "Orka VPN")"
ORKA_VPN_CERT="$(op get item --vault Verify --account gitlab --fields "Cert" "Orka VPN")"
ORKA_VPN_IP="$(op get item --vault Verify --account gitlab --fields url "Orka VPN")"

echo "Connecting to VPN. Enter sudo password..."
echo "$ORKA_VPN_PASS" | sudo openconnect "$ORKA_VPN_IP" --protocol=anyconnect --user="$ORKA_VPN_USER" --servercert "$ORKA_VPN_CERT" --passwd-on-stdin
