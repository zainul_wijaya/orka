#!/usr/bin/env python3
"""
Orka external inventory script
======================================

Generates Ansible inventory of Orka VMs.

The JSON information can be easily found in the cache file, whose default
location is /tmp/ansible-orka.cache).

The --pretty (-p) option pretty-prints the output for better human readability.

----
Although the cache stores all the information received from Orka,
the cache is not used for current VM information (in --list, --host).
This is so that accurate VM information is always
found.  You can force this script to use the cache with --force-cache.

----
Configuration is read from environment variables,
and then from command-line arguments.

Most notably, the Orka API Token must be specified. It can be specified
in the INI file or with the following environment variables:
    export ORKA_API_TOKEN='abc123' or
    export ORKA_API_KEY='abc123'

Alternatively, it can be passed on the command-line with --api-token.

----
```
usage: orka.py [-h] [--list] [--host HOST]
               [--pretty]
               [--cache-path CACHE_PATH]
               [--cache-max_age CACHE_MAX_AGE] [--force-cache]
               [--refresh-cache] [--env] [--api-token API_TOKEN]

Produce an Ansible Inventory file based on Orka credentials

optional arguments:
  -h, --help            show this help message and exit
  --list                List all active VMs as Ansible inventory
                        (default: True)
  --host HOST           Get all Ansible inventory variables about a specific VM
  --all                 List all Orka information as JSON
  --pretty, -p          Pretty-print results
  --cache-path CACHE_PATH
                        Path to the cache files (default: .)
  --cache-max_age CACHE_MAX_AGE
                        Maximum age of the cached items (default: 0)
  --force-cache         Only use data from the cache
  --refresh-cache, -r   Force refresh of cache by making API requests to
                        Orka (default: False - use cache files)
  --env, -e             Display ORKA_API_URL and ORKA_API_TOKEN
  --api-token API_TOKEN, -a API_TOKEN
                        Orka API Token
```

"""

# (c) 2013, Evan Wies <evan@neomantra.net>
# (c) 2017, Ansible Project
# (c) 2017, Abhijeet Kasurde <akasurde@redhat.com>
#
# Inspired by the EC2 inventory plugin:
# https://github.com/ansible/ansible/blob/devel/test/support/integration/plugins/inventory/aws_ec2.py
#
# This file is part of Ansible,
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

######################################################################

import argparse
import ast
import os
import re
import requests
import sys
from time import time

import json


class OrkaManager:
    def __init__(self, api_token):
        self.api_token = api_token
        self.api_endpoint = 'http://10.221.188.100'
        self.headers = {'Authorization': 'Bearer {0}'.format(self.api_token)}
        self.timeout = 60

    def _url_builder(self, path):
        if path[0] == '/':
            path = path[1:]
        return '%s/%s' % (self.api_endpoint, path)

    def send(self, url, method='GET', data=None):
        url = self._url_builder(url)
        data = json.dumps(data)
        try:
            if method == 'GET':
                resp_data = {}
                resp = requests.get(url,
                                    data=data,
                                    headers=self.headers,
                                    timeout=self.timeout)
                if resp.status_code != 200:
                    print(str(resp.status_code) + " " + resp.reason)
                    print(resp.text)
                    resp_data['virtual_machine_resources'] = []
                else:
                    json_resp = resp.json()

                    for key, value in json_resp.items():
                        if isinstance(value, list) and key in resp_data:
                            resp_data[key] += value
                        else:
                            resp_data[key] = value

        except ValueError as e:
            sys.exit("Unable to parse result from %s: %s" % (url, e))
        return resp_data

    def all_active_vms(self):
        resp = self.send('resources/vm/list')
        return resp['virtual_machine_resources']

    def show_vm(self, name):
        resp = self.send('resources/vm/status/%s' % name)
        return resp['virtual_machine_resources']


class OrkaInventory(object):

    ###########################################################################
    # Main execution path
    ###########################################################################

    def __init__(self):
        """Main execution path """

        # OrkaInventory data
        self.data = {}  # All Orka data
        self.inventory = {}  # Ansible Inventory

        # Define defaults
        self.cache_path = '.'
        self.cache_max_age = 0
        self.use_private_network = False
        self.group_variables = {}

        # Read settings, environment variables, and CLI arguments
        self.read_environment()
        self.read_cli_args()

        # Verify credentials were set
        if not hasattr(self, 'api_token'):
            msg = 'Could not find values for Orka api_token. They must be specified via either ini file, ' \
                  'command line argument (--api-token), or environment variables (ORKA_API_TOKEN)\n'
            sys.stderr.write(msg)
            sys.exit(-1)

        # env command, show Orka credentials
        if self.args.env:
            print("ORKA_API_URL=%s" % self.api_endpoint)
            print("ORKA_API_TOKEN=%s" % self.api_token)
            sys.exit(0)

        # Manage cache
        self.cache_filename = self.cache_path + "/ansible-orka.cache"
        self.cache_refreshed = False

        if self.is_cache_valid():
            self.load_from_cache()
            if len(self.data) == 0:
                if self.args.force_cache:
                    sys.stderr.write('Cache is empty and --force-cache was specified\n')
                    sys.exit(-1)

        self.manager = OrkaManager(self.api_token)

        # Pick the json_data to print based on the CLI command
        if self.args.host:
            json_data = self.load_vm_variables_for_host()
        else:  # '--list' this is last to make it default
            self.load_from_orka('resources/vm/list')
            self.build_inventory()
            json_data = self.inventory

        if self.cache_refreshed:
            self.write_to_cache()

        if self.args.pretty:
            print(json.dumps(json_data, indent=2))
        else:
            print(json.dumps(json_data))

    ###########################################################################
    # Script configuration
    ###########################################################################

    def read_environment(self):
        """ Reads the settings from environment variables """
        # Setup credentials
        if os.getenv("ORKA_API_URL"):
            self.api_endpoint = os.getenv("ORKA_API_URL")
        if os.getenv("ORKA_API_TOKEN"):
            self.api_token = os.getenv("ORKA_API_TOKEN")
        if os.getenv("ORKA_API_KEY"):
            self.api_token = os.getenv("ORKA_API_KEY")

    def read_cli_args(self):
        """ Command line argument processing """
        parser = argparse.ArgumentParser(description='Produce an Ansible Inventory file based on Orka credentials')

        parser.add_argument('--list', action='store_true', help='List all active VMs as Ansible inventory (default: True)')
        parser.add_argument('--host', action='store', help='Get all Ansible inventory variables about a specific VM')

        parser.add_argument('--pretty', '-p', action='store_true', help='Pretty-print results')

        parser.add_argument('--cache-path', action='store', help='Path to the cache files (default: .)')
        parser.add_argument('--cache-max_age', action='store', help='Maximum age of the cached items (default: 0)')
        parser.add_argument('--force-cache', action='store_true', default=False, help='Only use data from the cache')
        parser.add_argument('--refresh-cache', '-r', action='store_true', default=False,
                            help='Force refresh of cache by making API requests to Orka (default: False - use cache files)')

        parser.add_argument('--env', '-e', action='store_true', help='Display ORKA_API_TOKEN')
        parser.add_argument('--api-token', '-a', action='store', help='Orka API Token')

        self.args = parser.parse_args()

        if self.args.api_token:
            self.api_token = self.args.api_token

        # Make --list default if none of the other commands are specified
        if (not self.args.host):
            self.args.list = True

    ###########################################################################
    # Data Management
    ###########################################################################

    def load_from_orka(self, resource=None):
        """Get JSON from Orka API """
        if self.args.force_cache and os.path.isfile(self.cache_filename):
            return
        # We always get fresh VMs
        if self.is_cache_valid() and not (resource == 'resources/vm/list'
                                          or resource is None):
            return
        if self.args.refresh_cache:
            resource = None

        if resource == 'resources/vm/list' or resource is None:
            self.data['vms'] = self.manager.all_active_vms()
            self.cache_refreshed = True

    def add_inventory_group(self, key):
        """ Method to create group dict """
        host_dict = {'hosts': [], 'vars': {}}
        self.inventory[key] = host_dict
        return

    def add_host(self, group, host):
        """ Helper method to reduce host duplication """
        group = self.to_safe(group)

        if group not in self.inventory:
            self.add_inventory_group(group)

        if host not in self.inventory[group]['hosts']:
            self.inventory[group]['hosts'].append(host)
        return

    def build_inventory(self):
        """ Build Ansible inventory of VMs """
        self.inventory = {
            'all': {
                'hosts': [],
                'vars': self.group_variables
            },
            '_meta': {
                'hostvars': {}
            }
        }

        # add all VMs by id and name
        for vm_resource in self.data['vms']:
            if vm_resource['vm_deployment_status'] != 'Deployed':
                continue

            statuses = []
            try:
                statuses = vm_resource['status']
            except KeyError:
                print(vm_resource) # Debugging aid
                raise

            for status in statuses:
                if status['vm_status'] == 'running':
                    dest = status['virtual_machine_ip']
                    name = status['virtual_machine_name']
                else:
                    continue

                self.inventory['all']['hosts'].append(name)

                vars = {
                    'ansible_connection': 'ssh',
                    'ansible_host': dest,
                    'ansible_port': int(status['ssh_port']),
                }
                self.add_host('id_' + status['virtual_machine_id'], name)

                # groups that are always present
                for group in ('orka', 'owner_' + status['owner'],
                              'location_' + status['node_location']):
                    self.add_host(group, name)

                # hostvars
                info = self.orka_namespace(status)
                self.inventory['_meta']['hostvars'][name] = self.merge_two_dicts(info, vars)

    def load_vm_variables_for_host(self):
        """ Generate a JSON response to a --host call """
        vm_resources = self.manager.show_vm(self.args.host)
        for vm_resource in vm_resources:
            statuses = []
            try:
                statuses = vm_resource['status']
            except KeyError:
                print(vm_resource) # Debugging aid
                raise

            for status in statuses:
                if status['vm_status'] == 'running':
                    dest = status['virtual_machine_ip']
                else:
                    continue

                vars = {
                    'ansible_connection': 'ssh',
                    'ansible_host': dest,
                    'ansible_port': int(status['ssh_port']),
                }

                # hostvars
                info = self.merge_two_dicts(self.orka_namespace(status), vars)
                return {'vm': info}

        return {}

    ###########################################################################
    # Cache Management
    ###########################################################################

    def is_cache_valid(self):
        """ Determines if the cache files have expired, or if it is still valid """
        if os.path.isfile(self.cache_filename):
            mod_time = os.path.getmtime(self.cache_filename)
            current_time = time()
            if (mod_time + self.cache_max_age) > current_time:
                return True
        return False

    def load_from_cache(self):
        """ Reads the data from the cache file and assigns it to member variables as Python Objects """
        try:
            with open(self.cache_filename, 'r') as cache:
                json_data = cache.read()
            data = json.loads(json_data)
        except IOError:
            data = {'data': {}, 'inventory': {}}

        self.data = data['data']
        self.inventory = data['inventory']

    def write_to_cache(self):
        """ Writes data in JSON format to a file """
        data = {'data': self.data, 'inventory': self.inventory}
        json_data = json.dumps(data, indent=2)

        with open(self.cache_filename, 'w') as cache:
            cache.write(json_data)

    ###########################################################################
    # Utilities
    ###########################################################################
    @staticmethod
    def merge_two_dicts(x, y):
        z = x.copy()  # start with x's keys and values
        z.update(y)  # modifies z with y's keys and values & returns None
        return z

    @staticmethod
    def to_safe(word):
        """ Converts 'bad' characters in a string to underscores so they can be used as Ansible groups """
        return re.sub(r"[@\.\-]", "_", word)

    @staticmethod
    def orka_namespace(data):
        """ Returns a copy of the dictionary with all the keys put in a 'orka_' namespace """
        info = {}
        for k, v in data.items():
            info['orka_' + k] = v
        return info


###########################################################################
# Run the script
OrkaInventory()
