# Ansible Playbook: Setup OSX/iOS CI Environment

![MIT licensed][badge-license]

This playbook is meant to provision a MacStadium VM CI base image
(see inventory in `inventory/macstadium`) that can be used to manually scale
MacStadium CI hosts. It was inspired from MacStadium's macOS CI Setup
[Ansible Playbook](osx-playbook) and uses three external roles:

- Environment setup
  - [`elliotweiser.osx-command-line-tools`][osx-command-line-tools]
- Package management
  - [`geerlingguy.homebrew`][homebrew-role]
  - [`markosamuli.asdf`][asdf-role]

The target configuration is listed in `group_vars/all/*.yml`.

## Xcode & SDK version support

### Current Xcode supported versions

At the time of writing (12/2020), we support:

- Xcode 7.3.1
- Xcode 10.3
- Xcode 11.7
- Xcode 12.2

The versions below are unsupported because they [don't run on recent versions of OSX](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/issues/13):

- Xcode 8.3.3
- Xcode 9.4.1

### Supporting data

At the time of writing (12/2020), this table lists the latest versions of every Apple OS, their release date, and whether any hardware device is stuck at this OS version.
This helps inform which Xcode version we support.

This data has been compiled using [Mactracker](https://mactracker.ca/) and [Wikipedia](https://en.wikipedia.org/wiki/Xcode).

The necessity of building software with old versions of Xcode, rather than using the [deployment target setting](https://help.apple.com/xcode/mac/current/#/deve69552ee5), is explained by a customer [here](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/6564#note_462485395).

#### macOS

| Version       | Latest release | Year of latest release | Supplied by Xcode | Examples of devices stuck at this level                                                                                        |
|---------------|----------------|------|-------------------|--------------------------------------------------------------------------------------------------------------------------------|
| Lion          | 10.7.5         | 2012 | 4.6.3             | iMac (Late 2006), Mac Mini (Mid 2007), Macbook (Late 2008), Macbook Pro (Late 2006), Xserve (Early 2008)                       |
| Mountain Lion | 10.8.5         | 2013 | 5.1.1             | -                                                                                                                              |
| Mavericks     | 10.9.5         | 2014 | 6.4               | -                                                                                                                              |
| Yosemite      | 10.10.5        | 2015 | 6.4               | -                                                                                                                              |
| El Capitan    | 10.11.6        | 2016 | 7.3.1             | iMac (Mid 2009), Mac mini (Late 2009), Macbook (Mid 2009), Macbook Air (Mid 2009), Macbook Pro (Mid 2009), Xserve (Early 2009) |
| Sierra        | 10.12.6        | 2017 | 8.3.3             | -                                                                                                                              |
| High Sierra   | 10.13.6        | 2018 | 9.4.1             | iMac (Late 2011), Mac mini (Mid 2011), Macbook (Mid 2010), Macbook Air (Mid 2011), Macbook Pro (Late 2011)                     |
| Mojave        | 10.14.6        | 2019 | 10.3              | Mac Pro (Mid 2012)                                                                                                             |
| Catalina      | 10.15.7        | 2020 | 11.7 (also 12.1)  | iMac (Late 2013), Mac Mini (Late 2012), Macbook Air (Mid 2012), Macbook Pro (Early 2013)                                       |
| Big Sur       | 11.0.1         | 2020 | 12.2              | -                                                                                                                              |

#### iOS

| Version       | Latest release | Year of latest release | Supplied by Xcode | Examples of devices stuck at this level                                                                                        |
|---------------|----------------|------|-------------------|--------------------------------------------------------------------------------------------------------------------------------|
| 6             | 6.1.6          | 2014 | 4.6.3             | iPhone 3GS, iPod Touch (4th gen)                                                                                               |
| 7             | 7.1.2          | 2014 | 5.1.1             | iPhone 4, Apple TV (2nd gen)                                                                                                   |
| 8             | 8.4.2          | 2016 | 6.4               | Apple TV (3rd gen)                                                                                                             |
| 9             | 9.3.6          | 2019 | 7.3.1             | iPhone 4s, iPad 2, iPad (3rd Gen), iPad mini; iPöd Touch (5th Gen)                                                             |
| 10            | 10.3.4         | 2019 | 8.3.3             | iPhone 5, iPhone 5c, iPad (4th gen)                                                                                            |
| 11            | 11.4.1         | 2018 | 9.4.1             | -                                                                                                                              |
| 12            | 12.4.9         | 2020 | 10.3              | iPhone 6, iPhone 6s, iPod Touch (6th Gen), iPad Air, iPad Mini 2, iPad Mini 3                                                  |
| 13            | 13.7           | 2020 | 11.7              | -                                                                                                                              |
| 14            | 14.2.1         | 2020 | 12.2              | -                                                                                                                              |

#### iPad OS

| Version       | Latest release | Year of latest release | Supplied by Xcode | Examples of devices stuck at this level                                                                                        |
|---------------|----------------|------|-------------------|--------------------------------------------------------------------------------------------------------------------------------|
| 13            | 13.7           | 2020 | 11.7              | -                                                                                                                              |
| 14            | 14.2           | 2020 | 12.2              | -                                                                                                                              |

#### watchOS

| Version       | Latest release | Year of latest release | Supplied by Xcode | Examples of devices stuck at this level                                                                                        |
|---------------|----------------|------|-------------------|--------------------------------------------------------------------------------------------------------------------------------|
| 1             | 1.0.1          | 2015 | ?                 | -                                                                                                                              |
| 2             | 2.2.2          | 2016 | 7.3.1             | -                                                                                                                              |
| 3             | 3.2.3          | 2017 | 8.3.3             | -                                                                                                                              |
| 4             | 4.3.2          | 2018 | 9.4.1             | Watch (1st gen)                                                                                                                |
| 5             | 5.3.7          | 2020 | 10.3              | -                                                                                                                              |
| 6             | 6.2.9          | 2020 | 11.7              | Watch (Series 1 & Series 2)                                                                                                    |
| 7             | 7.1            | 2020 | 12.2              | -                                                                                                                              |

#### tvOS

| Version       | Latest release | Year of latest release | Supplied by Xcode | Examples of devices stuck at this level                                                                                        |
|---------------|----------------|------|-------------------|--------------------------------------------------------------------------------------------------------------------------------|
| 9             | 9.2.2          | 2016 | 7.3.1             | -                                                                                                                              |
| 10            | 10.2.2         | 2017 | 8.3.3             | -                                                                                                                              |
| 11            | 11.4.1         | 2018 | 9.4.1             | -                                                                                                                              |
| 12            | 12.4.1         | 2019 | 10.3              | -                                                                                                                              |
| 13            | 13.4.8         | 2020 | 11.7              | -                                                                                                                              |
| 14            | 14.2           | 2020 | 12.2              | -                                                                                                                              |

## Requirements

External roles come from [Ansible Galaxy][galaxy]. In order to install them,
you need to run:

```shell
ansible-galaxy install -r requirements.yml
```

<!-- markdownlint-disable MD013 -->
For the CI for this project to work, we need a VM image available at MacStadium
with SSH enabled. The VMs need to accept the SSH key used by this project
(defined in the [`.create_macstadium_img`](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/blob/2523f0a154c06556bcf2ad7bddee970577b13e7a/.gitlab-ci.yml#L75-77) job).
<!-- markdownlint-enable MD013 -->

Creating the build VM image:

```shell
# Enable SSH
sudo systemsetup -setremotelogin on

# Use that IP to copy your personal public key using ssh-copy-id
ssh-copy-id -i ~/.ssh/id_ed25519.pub admin@<MacStadium VM IP> -p 8822
```

### CI user

We use the [orka
API](https://orkadocs.macstadium.com/docs/api-quick-start) to provision
the VMs needed to build our images. A specific user inside of orka is
used to create these machines, with the username `ci@gitlab.com`. The
password of this user can be found in the 1Password vault `Verify` with
the name of `orka CI user`.

Use the [create
token](https://documenter.getpostman.com/view/6574930/S1ETRGzt?version=latest#3b0726f3-9473-4d42-bb3e-dfb242284cd2)
endpoint to create a new JWT token, and set it to `ORKA_API_TOKEN`
inside of the CI/CD settings.

## Usage

The idea for the closed beta period is to spin up a VM based on the image
generated from this repository.

In order to successfuly provision new customers, the following steps are required:

1. Install dependencies: `docker`, `jq`, `orka`, `op`, `openconnect`.

```shell
brew install homebrew/cask/docker jq orka 1password-cli openconnect
```

1. Go through the [Orka CLI Quick Start](https://orkadocs.macstadium.com/docs/quick-start).
   You should end up with your own user credentials, and be able to use the CLI
   to list and create VMs as shown in the tutorial.

1. Ensure `op` can access your company 1password account by running at least once:
   `op signin gitlab.1password.com YOU@gitlab.com`

1. Start the VPN connection in a separate shell with `./scripts/vpn.sh`

1. Run the provisioning script: `./scripts/closed_beta/provision.sh`.
   You will be asked for the customer name, the beta tracking issue, the xcode
   version they want to run and the base image to use.

1. Once the provisioning script has run, connect to a screen sharing session
   using the URL `vnc://...` that is shown at the end of the output.
   You can use `Screen Sharing.app` that comes with your Mac.

1. Once you can see the MacOS login screen, connect as `gitlab-runner`
   with the password `gitlab-runner`

1. Ensure the runner is online by opening the terminal and running
   `gitlab-runner verify`. You should see `Verify runner... is alive` in the output.

1. Leave the session open so users can run graphical applications such as the
   iOS Simulator in their CI, and close the Screen Sharing app.

1. Comment on the beta tracking issue that the runner is online using
   the template in the output.

### Debugging the job environment inside the VM

You can log in to the `gitlab-runner` account in which the customer jobs are run
by following the procedure below:

1. From the VNC terminal, log in to the `Admin` account
1. Open a terminal and `run sudo su -- gitlab-runner` (so it executes `su` as `root`)
1. Enter the `admin` account password (`${ANSIBLE_USER_UPDATED_PASSWORD}`)

```shell
$ whoami
gitlab-runner
```

### Local development environment

In order to successfuly run the playbook on your local machine during development,
the following steps are required:

1. Install dependencies: `ansible`, `jq`, `orka`

1. Go through the [Orka CLI Quick Start](https://orkadocs.macstadium.com/docs/quick-start).
   You should end up with your own user credentials, and be able to use the CLI
   to list and create VMs as shown in the tutorial.

1. Generate a key for the [`mac-runner-provisioning` GCP service account](https://console.cloud.google.com/iam-admin/serviceaccounts/details/116590241271226936536?authuser=1&project=group-verify-df9383)
   and store it in the project root directory as a JSON file (named like `group-verify-df9383-*.json`)

1. Create your own VM by running `scripts/dev-vm new`. By default, it is named after
   your local unix account. A [few more commands](#dev-vm-commands) exist.

1. Run ansible against your new VM with `scripts/dev-vm provision`.

1. Profit!

### dev-vm commands

| command | description |
| ------- | ----------- |
| `scripts/dev-vm status`    | Show the status of your VM |
| `scripts/dev-vm new`       | Create a new dev VM |
| `scripts/dev-vm destroy`   | Destroy the VM to start again |
| `scripts/dev-vm provision` | Run the local ansible playbook on your VM |

## Roles overview

### `ci_user`

Sets up a CI user. Defaults are located in `roles/ci_user/defaults/main.yml`.

### `os_prepare`

Applies latest OS updates, updates various defaults such as timezone and admin password.
Defaults are located in `roles/os_prepare/defaults/main.yml`.

### [`elliotweiser.osx-command-line-tools`][osx-command-line-tools]

Installs the Xcode Command Line Tools (containing for example Git).

### `xcode`

Based on [ansible-role-xcode](https://github.com/palmerc/ansible-role-xcode).

Installs the Xcode package specified by the following variables:

```yaml
    xcode_xip_gcs_bucket:
```

The name of the GCS bucket containing the Xcode xip file.

```yaml
    xcode_version:
```

The version of Xcode to install.

```yaml
    xcode_app_name:
```

The name of the folder that is expanded from the .xip file.
Normally `Xcode.app` or `Xcode-beta.app`.

```yaml
    xcode_app_dest_name:
```

The Xcode file name under `/Applications`. Normally `Xcode.app` or `Xcode-beta.app`.

#### GC storage

The xip file is located in the [`mac-runners` bucket](https://console.cloud.google.com/storage/browser/mac-runners;tab=objects?project=group-verify-df9383&prefix=),
and accessed through the [mac-runner-provisioning service account](https://console.cloud.google.com/iam-admin/serviceaccounts/details/116590241271226936536?project=group-verify-df9383).

The `GCS_KEYFILE` environment variable must contain the JSON string for
a service account key, downloaded from the Google Cloud dashboard.

### [`geerlingguy.homebrew`][homebrew-role]

Installs [Homebrew][homebrew], taps, casks and packages defined in the
variables reserved for that effect.

### [`markosamuli.asdf`][asdf-role]

Installs [asdf-vm][https://asdf-vm.com/] and packages defined in the `asdf_plugins`
variable.

### `runner`

Downloads and installs the GitLab Runner service. **NOTE: For the service start
to be successful, you need to sign in interactively to the target machine
(e.g. with the `ansible_user` account).**

## Variables

The playbook will need several variables in order to work correctly.
Their default values can be found in [`group_vars/all/*.yml`](group_vars/all/).

```yml
    ansible_user: admin
```

The default user name to use for installing and configuring the needed tooling.

```yml
    ansible_become_password: ""
```

The password used to gain sudo access.

```yml
    ansible_user_updated_password: ""
```

The updated password to be used for the `ansible_user`, used by the
`os_prepare` role.
This is the password that must be specified in `ansible_become_password`
when reconnecting after the `os_prepare` role has finished executing.

For information about the variables that the external roles
expect you can check their repositories:

- [`xcode`][xcode-role]
- [`geerlingguy.homebrew`][homebrew-role]
- [`markosamuli.asdf`][asdf-role]

## Dependencies

None.

## Example

```shell
    ansible-playbook site.yml \
        -i staging -l parallels-vm \
        -e ansible_user=${ANSIBLE_USER}
```

## GitLab provisioning pipeline

The CI pipeline defined in `.gitlab-ci.yml` contains jobs that will build
the final image in stages, adding increasing layers of software to it.
The following is a representation of the flow followed by the pipeline:

```mermaid
graph TB;
  style imgGeneration fill:#6e6e6e,color:#fff
  style prod fill:#6e6e6e,color:#fff
  style xcode fill:#4e4e4e,color:#fff

  subgraph imgProv["Image provisioning"]
    CIJob("CI job @<br/>
      gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka")
    CIJob-. "Docker CI container connects Ansible<br/>
      through SSH over VPN" .->BaseMacStadiumImg

    subgraph imgGeneration["MacStadium k8s cluster"]
      BaseMacStadiumImg["Base MacStadium Catalina image"]-->BaseGitLabImg;
      BaseGitLabImg["Base GitLab OS image (mr-123-gitlab-base.img)
        <br/>(OS updates, add user, set timezone, etc.)"]
      BaseRunnerImg["Base GitLab Runner tools image (mr-123-toolchain.img)<br/>
        (Homebrew, asdf, Ruby, etc.)"]

      BaseGitLabImg-->BaseRunnerImg;
      BaseRunnerImg-->XC1;
      BaseRunnerImg-->XC2;
      BaseRunnerImg-->XC3;
      BaseRunnerImg-->XC4;
      BaseRunnerImg-->XC5;
      click BaseRunnerImg "https://gitlab.com/gitlab-org/gitlab-runner/-/issues/6564#content-of-virtual-machine-image-for-closed-beta"

      subgraph xcode["Dynamic pipeline build"]
        XC1["Xcode 12.6 beta 6 image<br/>(.xip from GCS)"]
        XC2["Xcode 11.6 image<br/>(.xip from GCS)"]
        XC3["Xcode 11.5 image<br/>(.xip from GCS)"]
        XC4["Xcode 11.4.1 image<br/>(.xip from GCS)"]
        XC5["Xcode x.y.z image<br/>(.xip from GCS)"]
        XC1_RunnerImg["GitLab Runner image<br/>`xcode-12.6b6-10.15.img`"]
        XC2_RunnerImg["GitLab Runner image<br/>`xcode-11.6-10.15.img`"]
        XC3_RunnerImg["GitLab Runner image<br/>`xcode-11.5-10.15.img`"]
        XC4_RunnerImg["GitLab Runner image<br/>`xcode-11.4.1-10.15.img`"]
        XC5_RunnerImg["GitLab Runner image<br/>`xcode-x.y.z-10.15.img`"]

        XC1-->XC1_RunnerImg
        XC2-->XC2_RunnerImg
        XC3-->XC3_RunnerImg
        XC4-->XC4_RunnerImg
        XC5-->XC5_RunnerImg

        click XC1 "https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/jobs/713023098#L361"
        click XC2 "https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/jobs/713023098#L592"
        click XC3 "https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/jobs/713023098#L820"
        click XC4 "https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/jobs/713023098#L1026"
      end

      XC1_RunnerImg-->XC1_tag("🏷 xcode-12.6");
      XC2_RunnerImg-->XC2_tag("🏷 xcode-11.6");
      XC3_RunnerImg-->XC3_tag("🏷 xcode-11.5");
      XC4_RunnerImg-->XC4_tag("🏷 xcode-11.4.1");
      XC5_RunnerImg-->XC5_tag("🏷 xcode-x.y.z");
    end
  end

  subgraph prod["Production (MacStadium k8s cluster)"]
    RunnerManagerInsts(("GitLab Runner Manager instances<br/>
      (1 runner dedicated per customer)"))

    XC1_tag-.->RunnerManagerInsts
    XC2_tag-.->RunnerManagerInsts
    XC3_tag-.->RunnerManagerInsts
    XC4_tag-.->RunnerManagerInsts
    XC5_tag-.->RunnerManagerInsts
  end

  style gitlab fill:#6349db,color:#fff
  subgraph gitlab["GitLab"]
    RunnerManagerInsts-. poll .->Customer1Job["Customer #1 CI job"]
    RunnerManagerInsts-. poll .->Customer2Job["Customer #2 CI job"]
  end
```

### Output image names

The following is an overview of the names of the output image created
by each job, and used as the base image by the job in the subsequent stage:

<!-- markdownlint-disable MD013 -->
| Job                         | MR output image name           | Non-MR output image name        |
| --------------------------- | ------------------------------ | ------------------------------- |
| `create_gitlab_base_img`    | `mr-123-gitlab-base.img`       | `gitlab-base-10.15-beta.img`    |
| `create_toolchain_base_img` | `mr-123-toolchain.img`         | `toolchain-10.15-beta.img`      |
| `create_runner_11.5_img`    | `mr-123-runner-11.5.img`       | `runner-11.5-10.15-beta.img`    |
<!-- markdownlint-enable MD013 -->

### Notes

- Before saving a VM image, it is important to reboot the VM so that
  all data is flushed to disk and captured   by the `orka vm save` command.
  This is taken care of by the `orka-save-vm-image` Makefile target by issuing
  an Ansible `reboot` command.
- After a MR is merged, the images from the MR will not be automatically deleted.
  They should be manually purged after a merge using the following script (using
  MR !6 as an example):

  ```shell
  MR=6
  orka image list --json | \
    jq -r ".images[] | select(contains(\"mr-${MR}-\"))" | \
    xargs -I{} orka image delete -y --image {}
  ```

## License

[MIT][link-license]

## Author Information

This playbook was created in 2020 by GitLab.

### Maintainer(s)

- [Pedro Pombeiro](https://gitlab.com/pedropombeiro)

[ansible]: https://docs.ansible.com/ansible/2.4/index.html
[galaxy]: https://galaxy.ansible.com/
[badge-license]: https://img.shields.io/badge/License-MIT-green.svg
[link-license]: https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/raw/master/LICENSE
[homebrew]: https://brew.sh/
[homebrew-role]: https://github.com/geerlingguy/ansible-role-homebrew
[osx-playbook]: https://github.com/macstadium/ansible-playbook-osx-ci-setup
[asdf-role]: https://galaxy.ansible.com/markosamuli/asdf
[osx-command-line-tools]: https://github.com/elliotweiser/ansible-osx-command-line-tools
